#!/bin/zsh

XML='xmlstarlet'
ATTRIBUTS_XSL="$(cd $(dirname $0) && pwd)/attributs.xsl"
PROGRAM="$(basename $0)"

function flatten_formalism() {
    fml_file=$1
    flattener="tmp/flatten_formalism.xsl"
    test -e $flattener || wget -q -O "$flattener" "https://teamcity-systeme.lip6.fr/guestAuth/repository/download/bt72/.lastSuccessful/classes/flatten_formalism.xsl" && sed -i 's/stylesheet version="2.0"/stylesheet version="1.0"/' "$flattener"
    $XML tr --xinclude "$flattener" "$fml_file" >| "${fml_file/fml/flat.fml}"
}

function layout() {
    fml="$1"
    $XML tr "$ATTRIBUTS_XSL" "$fml"
}

function usage() {
    echo "Usage: $PROGRAM [options] formalism(s)"
    echo "Options:"
    echo "\t--flatten\tFlatten the formalism before the layout"
    echo "\t--pdf\tProduce a pdf file"
    echo "\t-h,--help\tDisplay this message"
}

function main() {
    args=()
    flatten=false;
    pdf=false;
    output="cat"
    verbose=true;
    debug=false;

    while [ $1 ]; do
        case "$1" in
            -h|--help) usage; exit 0;;
            -f|--flatten) flatten=true;;
            --pdf) pdf=true;;
            -q|--quiet) verbose=false;;
            -d) debug=true;;
            -*) echo "option $1 doesn't exist"; usage; exit 1;;
            *) args+=("$1");;
        esac
        shift
    done
    mkdir tmp
    for fml in $args
    do
        $flatten && flatten_formalism "$fml" && fml="${fml/fml/flat.fml}"
        $debug && cat $fml
        echo $fml
        dot_content=$(layout "$fml")
#        $flatten && rm -f "$fml"
#        $flatten && mv "fml" "flat/$fml"
        $verbose && echo $dot_content
        $pdf && dot -o"$fml.pdf" -Tpdf <(echo "$dot_content")
    done
    rm -rf tmp
}

main $@
