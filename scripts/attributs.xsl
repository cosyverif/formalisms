<?xml version='1.0' encoding='UTF-8'?>

<!DOCTYPE stylesheet [
  <!ENTITY cr "
">
]>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:fml="http://cosyverif.org/ns/formalism"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron">

    <xsl:output method="text" indent="no"/>

    <xsl:key
        name="attributeByName"
        match="fml:leafAttribute | fml:complexAttribute"
        use="@name" />
    <xsl:key
        name="elementByName"
        match="fml:formalism | fml:nodeType | fml:arcType"
        use="@name" />

    <xsl:template name="element">
        <xsl:text>  </xsl:text>
        <xsl:value-of select="generate-id(key('elementByName', @name))"/>
        <xsl:text> [color=red,style=filled,fontcolor=white,label="</xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>"]</xsl:text>
        <xsl:text>;&cr;</xsl:text>
    </xsl:template>

    <xsl:template match="fml:formalism">
        <xsl:text>digraph formalisms {&cr;</xsl:text>
        <xsl:text>  splines=ortho;&cr;</xsl:text>
        <xsl:text>  layout=dot;&cr;</xsl:text>
        <xsl:text>  edge [penwidth=0.5,labeldistance=1.7,labelangle=-38.0,labelfloat=true,arrowsize=0.5,labelfontsize=8.0];&cr;</xsl:text>
        <xsl:text>  node [shape=box];&cr;&cr;</xsl:text>
        <xsl:call-template name="element"/>
        <xsl:apply-templates/>
        <xsl:text>}&cr;</xsl:text>
    </xsl:template>

    <xsl:template match="fml:nodeType | fml:arcType">
        <xsl:call-template name="element"/>
    </xsl:template>

    <xsl:template name="reftype">
            <xsl:text>  </xsl:text>
            <xsl:value-of select="generate-id(key('elementByName', @refType))"/>
            <xsl:if test="count(key('elementByName', @refType)) = 0">
                <xsl:text>ref</xsl:text>
                <xsl:value-of select="generate-id()"/>
                <xsl:text> [color=grey,fontcolor=grey,label="</xsl:text>
                <xsl:value-of select="@refType"/>
                <xsl:text>"]</xsl:text>
                <xsl:text>;&cr;</xsl:text>
                <xsl:text>  </xsl:text>
                <xsl:text>ref</xsl:text>
                <xsl:value-of select="generate-id()"/>
            </xsl:if>
            <xsl:text> -> </xsl:text>
            <xsl:value-of select="generate-id(key('attributeByName', @name)[@refType=current()/@refType or (not(@refType) and not(current()/@refType))])"/>
            <xsl:text> [dir=both,arrowhead=none,arrowtail=none,penwidth=1,style="dotted,bold"];&cr;</xsl:text>
    </xsl:template>

    <xsl:template name="attribute">
        <xsl:param name="shape">box</xsl:param>

        <xsl:if test="@refType or (not(@refType) and //fml:child[@refName = current()/@name])">
            <xsl:call-template name="attribute2">
                <xsl:with-param name="shape" select="$shape"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="attribute2">
        <xsl:param name="shape">box</xsl:param>

        <xsl:text>  </xsl:text>
        <xsl:value-of select="generate-id(key('attributeByName', @name)[@refType=current()/@refType or (not(@refType) and not(current()/@refType))])"/>
        <xsl:text> [shape=</xsl:text>
        <xsl:value-of select="$shape"/>
        <xsl:text>,label="</xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>"]</xsl:text>
        <xsl:text>;&cr;</xsl:text>

        <xsl:if test="@refType">
            <xsl:call-template name="reftype"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="fml:complexAttribute[not(@combineChild) or @combineChild='inteleave']">
        <xsl:call-template name="attribute"/>
    </xsl:template>

    <xsl:template match="fml:complexAttribute[@combineChild='choice']">
        <xsl:call-template name="attribute">
            <xsl:with-param name="shape">octagon</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="fml:leafAttribute">
        <xsl:call-template name="attribute">
            <xsl:with-param name="shape">oval</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- liens childs -> attributs -->
    <xsl:template match="fml:child">
        <xsl:choose>
            <xsl:when test="count(key('attributeByName', @refName)) = 0">
                <xsl:text>  </xsl:text>
                <xsl:value-of select="generate-id()"/>
                <xsl:text>[color=grey,fontcolor=grey,label="</xsl:text>
                <xsl:value-of select="@refName"/>
                <xsl:text>"]</xsl:text>
                <xsl:text>;&cr;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="attr" select=".."/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="count(preceding::fml:child[@refName = current()/@refName])"/>
                <xsl:text>&cr;</xsl:text>
<!--                <xsl:for-each select="key('attributeByName', @refName)[not(@refType)]">-->
<!--                    <xsl:text>#</xsl:text>-->
<!--                    <xsl:value-of select="@name"/>-->
<!--                    <xsl:text>&cr;</xsl:text>-->
<!--                    <xsl:call-template name="attribute2">-->
<!--                        <xsl:with-param name="shape">box</xsl:with-param>-->
<!--                    </xsl:call-template>-->
<!--                    <xsl:apply-templates select="fml:child[generate-id() != generate-id(current())]"/>-->
<!--                </xsl:for-each>-->
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>  </xsl:text>
        <xsl:value-of select="generate-id(..)"/>
        <xsl:text> -> </xsl:text>
        <xsl:value-of select="generate-id(key('attributeByName', @refName)[not(@refType)])"/>
        <xsl:if test="count(key('attributeByName', @refName)) = 0">
            <xsl:value-of select="generate-id()"/>
            <xsl:text> </xsl:text>
        </xsl:if>

        <xsl:text> [headlabel="</xsl:text>
        <xsl:choose>
            <xsl:when test="@minOccurs"><xsl:value-of select="@minOccurs"/></xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
        <xsl:text>:</xsl:text>
        <xsl:choose>
            <xsl:when test="@maxOccurs"><xsl:value-of select="@maxOccurs"/></xsl:when>
            <xsl:otherwise>∞</xsl:otherwise>
        </xsl:choose>
        <xsl:text>"]</xsl:text>
        <xsl:text>;&cr;</xsl:text>
    </xsl:template>

    <xsl:template match="node()" priority="-1">
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>
