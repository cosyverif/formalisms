#!/bin/zsh

folder=$(cd -P -- $(dirname $0) && dirname $(pwd))
formalism_schema=$folder/fml/schemas/formalism.rng
model_schema=$folder/grml/schemas/model.rng

# Validate all formalisms
for formalism in $(find $folder -name '*.fml'); do
    testname="$(basename $formalism)"
    echo "##teamcity[testStarted name='$testname' captureStandardOutput='true']"
    xmllint --noout --relaxng $formalism_schema $formalism
    if [ $? -ne 0 ]; then
        echo "##teamcity[testFailed name='$testname' message='This formalism is not correct']"
    fi
    echo "##teamcity[testFinished name='$testname']"
done

# Validate all models
for model in $(find $folder -name '*.grml'); do
    testname="$(basename $model)"
    echo "##teamcity[testStarted name='$testname' captureStandardOutput='true']"
    xmllint --noout --relaxng $model_schema $model
    if [ $? -ne 0 ]; then
        echo "##teamcity[testFailed name='$testname' message='This model is not correct']"
    fi
    echo "##teamcity[testFinished name='$testname']"
done

# All .gml must be renamed
for model in $(find $folder -name '*.gml'); do
    testname="$(basename $model)"
    echo "##teamcity[testStarted name='$testname' captureStandardOutput='true']"
    echo "##teamcity[testFailed name='$testname' message='This model must be renamed']"
    echo "##teamcity[testFinished name='$testname']"
done
