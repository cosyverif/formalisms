___
# test.sh
## Description : 

This script validates all formalisms (FML) and models (GRML) using their respective RelaxNG (RNG) schemas. It also checks that all models with a .gml extension have been renamed to .grml.

## Usage : 

./test.sh

## Requirement : 
- xmllint
___
# formalisms.sh
## Description : 

This script is used to generate a visual representation under pdf of all the formalisms (FML) present on the formalisms directory.

## Usage :

./formalism.sh

## Requirements : 
- zsh
- xmlstarlet
- wget
- dot (from Graphviz)
___
# attributs.sh
## Description : 

This script takes one or more formalism files and creates for each formalism, a graph of attributes and elements in DOT language. You can choose the --pdf option to convert the DOT output to a PDF file.

## Usage : 

./attributs.sh [options] formalism(s)

Options:
        --flatten       	Flatten the formalism before the layout
        --pdf   		Produce a pdf file
        -h,--help       	Display this message

## Requirements : 
- zsh
- xmlstarlet
- wget
- dot (from Graphviz)
