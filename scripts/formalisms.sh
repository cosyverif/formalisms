#!/bin/zsh

cd $(dirname $0)

dot_file="formalisms.dot"
fml_ns="http://cosyverif.org/ns/formalism"
xml=xmlstarlet

function print_header() {
    echo "digraph formalisms {"
    echo "  splines=ortho;"
    echo "  layout=dot;"
#    echo "  concentrate=true;"
    echo "  node [shape=box];"
    echo ""
}

function print_footer() {
    echo ""
    echo "}"
    echo ""
}

function xpath_value() {
    xpath=$1
    fml_file=$2
    $xml sel -T -N xi="http://www.w3.org/2001/XInclude" -N fml=$fml_ns -t -v "$xpath" "$fml_file"
}

function get_id() {
    echo ${1//[-\.]/_}
}

function flatten_formalism() {
    fml_file=$1
    flattener="tmp/flatten_formalism.xsl"
    test -e $flattener || wget -q -O "$flattener" "https://teamcity-systeme.lip6.fr/guestAuth/repository/download/bt72/.lastSuccessful/classes/flatten_formalism.xsl" && sed -i 's/stylesheet version="2.0"/stylesheet version="1.0"/' "$flattener"
    $xml tr --xinclude "$flattener" "$fml_file" >| "tmp/$fml_file"
}

function print_formalism() {
    fml_file=$1
    id=$(get_id "$fml_file")
    flatten_formalism "$fml_file"

    # create a node
    echo -n "  $id [label=<"
    echo "<table border='0' cellborder='0' cellspacing='0'>"
    echo "<tr><td><b>$(xpath_value "/fml:formalism/@name" "$fml_file")</b></td></tr>"

    IFS="
"

    # Export node types
    bool="false"
    for nodeType in $(xpath_value "//fml:nodeType/@name" "$fml_file")
    do
        bool="true"
        echo "<tr><td align='left'>node: <i>$nodeType</i></td></tr>"
    done
    $bool && echo "<tr><td height='5'></td></tr>"

    # Export arc types
    bool="false"
    for arcType in $(xpath_value "//fml:arcType/@name" "$fml_file")
    do
        bool="true"
        echo "<tr><td align='left'>arc: <i>$arcType</i></td></tr>"
    done
    $bool && echo "<tr><td height='5'></td></tr>"

    # Export complex attributes
    bool="false"
    complexAttributes=($($xml tr "test.xsl" "$fml_file"))
    for i in $(seq 1 2 ${#complexAttributes})
    do
        attribute=$complexAttributes[$i]
        refType=$complexAttributes[$i+1]
        bool="true"
        echo -n "<tr><td align='left'>ca: <i>$attribute</i>"
        separator=""
        if [ "$refType" != " " ]
        then
            echo " → <font color='red'>$refType</font>"
            echo -n " ["
            for child in $(xpath_value "//fml:complexAttribute[@name=\"$attribute\" and @refType=\"$refType\"]/fml:child/@refName" "$fml_file")
            do
                echo -n "$separator$child"
                separator=", "
            done
        else
            echo -n " ["
            for child in $(xpath_value "//fml:complexAttribute[@name=\"$attribute\"]/fml:child/@refName" "$fml_file")
            do
                echo -n "$separator$child"
                separator=", "
            done
        fi
        echo -n "]"
        echo "</td></tr>"
    done
    $bool && echo "<tr><td height='5'></td></tr>"

    # Export leaf attributes
    leafAttributes=($($xml tr "test2.xsl" "$fml_file"))
    for i in $(seq 1 2 ${#leafAttributes})
    do
        attribute=$leafAttributes[$i]
        refType=$leafAttributes[$i+1]
        if [ "$refType" != " " ]
        then
            echo "<tr><td align='left'>la: <i>$attribute</i> → <font color='red'>$refType</font></td></tr>"
        else
            echo "<tr><td align='left'>la: <i>$attribute</i></td></tr>"
        fi
    done
    unset IFS

    echo "</table>>];"

    # for each include add an edge
    for include in $(xpath_value "//xi:include/@href" "$fml_file")
    do
        echo "  $id -> $(get_id $include);"
    done
    echo
}

mkdir tmp
{
print_header

for formalism in `find "$(dirname $(pwd))" -name '*.fml'`
do
    print_formalism $formalism
done
#print_formalism snb.fml

print_footer
#} >| $dot_file
} | dot -Tpdf -oout.pdf
#}
rm -rf tmp
